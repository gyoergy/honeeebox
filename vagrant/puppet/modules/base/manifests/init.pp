
# Class: base::tuning
#
#
class base::tuning {

    exec { "sysctl":
        command => "/sbin/sysctl -w kernel.shmmax=46088192 && touch /etc/_sysctl_tuned",
        creates => "/etc/_sysctl_tuned"
    }

    file { "/etc/sysctl.conf":
        ensure => file,
        owner  => "root", 
        group  => "root",
        source => "puppet:///modules/backend/etc/sysctl.conf" 
    }
}


# Class: base::postgres
#
#
class base::postgres {
    require base::tuning

    package { "postgresql": 
        ensure  => present,
    }

    file { "/etc/postgresql/9.1/main/postgresql.conf":
        ensure  => "present",
        owner   => "postgres", 
        group   => "postgres",
        recurse => true,
        source  => "puppet:///modules/backend/etc/postgresql/9.1/main/postgresql.conf",
        require => Package["postgresql"],
        notify  => Exec["restart-postgres"]
    }     

    file { "/etc/postgresql/9.1/main/pg_hba.conf":
        ensure  => "present",
        owner   => "postgres", 
        group   => "postgres",
        recurse => true,
        source  => "puppet:///modules/backend/etc/postgresql/9.1/main/pg_hba.conf",
        require => Package["postgresql"],
        notify  => Exec["restart-postgres"]
    }     

    exec { "restart-postgres":
        command => "/etc/init.d/postgresql restart",
        refreshonly => true
    }

}

