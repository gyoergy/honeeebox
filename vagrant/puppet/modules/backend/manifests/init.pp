
# Class: backend::files
#
# keep this lot in a separate class so we can ensure they are all 
# installed before the exec() anything
#
class backend::files {

    $targetuser=$backend::targetuser

    file { "/opt/honeynet/bin":
        ensure => directory,
        recurse => true,
        owner   => "root",
        group   => "root",
        source => "puppet:///modules/backend/opt/honeynet/bin"
    }  

    file { "/opt/hbbackend":
        ensure  => present,
        owner   => $targetuser,
        group   => $targetuser,
        recurse => true,
        source  => "puppet:///modules/backend/opt/hbbackend"
    }

    file { "/opt/hbbackend/conf/local.conf":
        ensure => file,
        owner   => $targetuser,
        group   => $targetuser,
        source => "puppet:///modules/backend/opt/hbbackend/conf/local.conf"
    }   

    file { "/opt/honeynet":
        ensure => present,
        source => "puppet:///modules/backend/opt/honeynet",
        recurse => true,
        owner   => "root",
        group   => "root"
    }   
   
    file { "/etc/environment":
        ensure => present,
        owner  => "root", 
        group  => "root", 
        source => "puppet:///modules/backend/etc/environment"
    }

    file { "/opt/glassfish3":
        ensure => directory,
        owner  => "root",
        group  => "root"
    }

    file { "/etc/init.d/hbbackend":
        ensure => present,
        owner  => "root", 
        group  => "root",
        mode   => "0755",
        source => "puppet:///modules/backend/etc/init.d/hbbackend"
    }

    file { "/etc/rc0.d/S20hbbackend":
        ensure  => link,
        target  => "/etc/init.d/hbbackend",
        require => File["/etc/init.d/hbbackend"]
    }

    file { "/opt/honeynet/bin/create-hbbackend-domain.sh":
        ensure => file,
        mode   => 755,
        owner  => root,
        group  => root,
        content => template("backend/create-hbbackend-domain.erb")
    }

}

# Class: backend::packages
#
#
class backend::packages {

    exec { "/usr/bin/aptitude -y install --without-recommends openjdk-7-jdk ant ivy maven && touch /etc/_java_packages_installed": 
        creates => "/etc/_java_packages_installed" 
    }

    package { "unzip": ensure => present }
}


# we use 20,000 seconds as a totally arbitary below
# should give enough time to download even on slow broadband without being
# infinite (0)

class backend::glassfish {
    require backend::files
    require backend::packages

    exec { "/opt/honeynet/bin/install-glassfish.sh":
        timeout => 20000,
    	user => "root",
    	group => "root",
        creates => "/etc/_glassfish_installed"
    }

}

class backend {
    # user to run the services as
    #
    $targetuser="vagrant"

    require backend::files
    require backend::packages
    require base::postgres
    require backend::glassfish

    exec { "create-hbbackend-db.sh":
    	command => "/opt/honeynet/bin/create-hbbackend-db.sh", 
    	user => "root",
    	group => "root",
        creates => "/etc/_hbbackend_db_created"
    }

    exec { "create-hbbackend-domain.sh":
    	command => "/opt/honeynet/bin/create-hbbackend-domain.sh",
        user => "root",
    	group => "root",
        creates => "/etc/_hbbackend_domain_created"
    }

    # not needed for now as we use a different ip->geo loookup
    #
    # exec { "install-geoip.sh":
    # 	command => "/usr/bin/sudo -u ${targetuser} /opt/honeynet/bin/install-geoip.sh",
    #     timeout => 20000,
    #     creates => "/opt/hbbackend/geoip/GeoLiteCity.dat"
    # }

    exec { "download-hbbackend-deps.sh":
        command => "/usr/bin/sudo -u ${targetuser} /opt/honeynet/bin/download-hbbackend-deps.sh",
        timeout => 600,
        creates => ["/repo/hbbackend/lib2/*.jar", "/repo/hbbackend/lib"],
    }
    
    exec { "setup-hbbackend-domain.sh":
    	command => "/usr/bin/sudo -i -u ${targetuser} /opt/honeynet/bin/setup-hbbackend-domain.sh",
        timeout => 600,
        require => [Exec["create-hbbackend-db.sh"], Exec["create-hbbackend-domain.sh"],
		    Exec["download-hbbackend-deps.sh"], ],
        creates => "/etc/_hbbackend_domain_set_up"
    }

    exec { "build-hbbackend.sh":
        command => "/usr/bin/sudo -u ${targetuser} /opt/honeynet/bin/build-hbbackend.sh",
        timeout => 600,
    	require => Exec["download-hbbackend-deps.sh"],
    }

    exec { "deploy-backend.sh":
        command => "/usr/bin/sudo -i -u ${targetuser} /opt/honeynet/bin/deploy-backend.sh",
        timeout => 600,
        require => [ Exec["setup-hbbackend-domain.sh"], Exec["build-hbbackend.sh"]  ],
        creates => "/etc/_deploy_backend_run"
    }

    service { "hbbackend":
        hasrestart => true,
        enable => true,
        require => Exec["setup-hbbackend-domain.sh"]
    }

}
