#!/bin/sh -e

if [ ! -f /etc/_hbbackend_domain_set_up ]
then

PATH="/opt/glassfish3/bin:$PATH"
export PATH

# copy deps
libdir=/repo/hbbackend/lib2
cp $libdir/postgresql-*.jdbc4.jar \
        $libdir/hpfeeds-java-*.jar \
/opt/glassfish3/glassfish/domains/hbbackend/lib

libdir=/repo/hbbackend/lib
cp $libdir/xadisk-*.jar \
        $libdir/xadisk-*.rar \
        $libdir/slf4j-api-*.jar \
        $libdir/logback-core-*.jar \
        $libdir/logback-classic-*.jar\
        $libdir/concurrentlinkedhashmap-lru-*.jar \
/opt/glassfish3/glassfish/domains/hbbackend/lib


# read hpfeeds credentials from local.conf
hpfeedsra_config=$(
cat /opt/hbbackend/conf/local.conf  |
sed -n \
        -e '/hpfeedsra.host/s//HPFEEDSRA_HOST/1p' \
        -e '/hpfeedsra.port/s//HPFEEDSRA_PORT/1p' \
        -e '/hpfeedsra.ident/s//HPFEEDSRA_IDENT/1p' \
        -e '/hpfeedsra.secret/s//HPFEEDSRA_SECRET/1p' \
        -e '/hpfeedsra.channels/s//HPFEEDSRA_CHANNELS/1p'  |
{
        IFS=' ='
        while read prop val ; do
                [ -z "$prop" ] && { echo 'error: malformed input' >&2 ; exit 1 ;}
                [ -z "$val" ] && { echo 'error: malformed input' >&2; exit 1 ;}
                echo "$prop $val"
        done
}
)


# start
asadmin --port 9948 start-domain hbbackend

# postgres connection pools and jdbc
asadmin --port 9948 <<EOL
create-jdbc-connection-pool --datasourceclassname org.postgresql.xa.PGXADataSource --restype javax.sql.XADataSource --property user=hbbackend:password=hbbackend:databaseName=hbbackend:serverName=localhost:port=5432 PgPool_hbbackend
ping-connection-pool PgPool_hbbackend
create-jdbc-resource --connectionpoolid PgPool_hbbackend jdbc/hbbackend

create-jdbc-connection-pool --datasourceclassname org.postgresql.xa.PGXADataSource --restype javax.sql.XADataSource --property user=hbstats:password=hbstats:databaseName=hbbackend:serverName=localhost:port=5432 --steadypoolsize 1 --maxpoolsize 4 PgPool_hbstats
ping-connection-pool PgPool_hbstats
create-jdbc-resource --connectionpoolid PgPool_hbstats jdbc/hbstats
EOL

# thread pools
# and then restart the domain for the thread pools to become available
asadmin --port 9948 <<EOL
create-threadpool --minthreadpoolsize=5 --maxthreadpoolsize=50 xadisk-thread-pool
create-threadpool --minthreadpoolsize=16 --maxthreadpoolsize=16 hpfeedsra-thread-pool
restart-domain hbbackend
EOL

# xadisk
asadmin --port 9948 <<EOL
create-resource-adapter-config --threadpoolid xadisk-thread-pool --property xaDiskHome=/opt/hbbackend/xadisk:instanceId=hbbackend xadisk
deploy --name xadisk /opt/glassfish3/glassfish/domains/hbbackend/lib/xadisk-1.2.1.rar
create-connector-connection-pool --raname xadisk --connectiondefinition org.xadisk.connector.outbound.XADiskConnectionFactory --property instanceId=hbbackend --transactionsupport XATransaction xadisk/ConnectionFactory
ping-connection-pool xadisk/ConnectionFactory
create-connector-resource --poolname xadisk/ConnectionFactory xadisk/ConnectionFactory
EOL

# jms stomp bridge and disable autocreate
asadmin --port 9948 <<EOL
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.bridge\\\\.enabled=true
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.bridge\\\\.activelist=stomp
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.bridge\\\\.admin\\\\.user=admin
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.bridge\\\\.admin\\\\.password=admin
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.bridge\\\\.stomp\\\\.tcp\\\\.port=9972
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.autocreate\\\\.queue=false
set configs.config.server-config.jms-service.jms-host.default_JMS_host.property.imq\\\\.autocreate\\\\.topic=false
EOL

# jms destinations
asadmin --port 9948 <<EOL
create-jmsdest --desttype topic new_attack
create-jmsdest --desttype topic new_binary
create-jmsdest --desttype topic new_binary_stored
create-jmsdest --desttype topic new_ip
create-jms-resource --restype javax.jms.Topic --property Name=new_attack jms/new_attack
create-jms-resource --restype javax.jms.Topic --property Name=new_binary jms/new_binary
create-jms-resource --restype javax.jms.Topic --property Name=new_binary_stored jms/new_binary_stored
create-jms-resource --restype javax.jms.Topic --property Name=new_ip jms/new_ip
EOL

# jms connection factories
asadmin --port 9948 <<EOL
create-jms-resource --restype javax.jms.ConnectionFactory jms/ConnectionFactory
create-jms-resource --restype javax.jms.ConnectionFactory --property ClientId=virustotal jms/DurableConsumer/virustotal
create-jms-resource --restype javax.jms.ConnectionFactory --property ClientId=shadowserver_asn jms/DurableConsumer/shadowserver_asn
create-jms-resource --restype javax.jms.ConnectionFactory --property ClientId=shadowserver_geoip jms/DurableConsumer/shadowserver_geoip
ping-connection-pool jms/ConnectionFactory
EOL

# logback config location and monitoring
asadmin --port 9948 <<EOL
create-jvm-options -Dlogback.configurationFile=/opt/hbbackend/conf/logback.xml
enable-monitoring --modules connector-connection-pool=HIGH:connector-service=HIGH:deployment=HIGH:ejb-container=HIGH:http-service=HIGH:jdbc-connection-pool=HIGH:jms-service=HIGH:jvm=HIGH:thread-pool=HIGH:transaction-service=HIGH:jms-service=HIGH:web-container=HIGH
EOL

# finally, restart once again for a clean start
asadmin --port 9948 restart-domain hbbackend


# create hpfeedsra config
while read prop val ; do
        eval "$prop=\"$val\""
done << EOL
$hpfeedsra_config
EOL

asadmin --port 9948 <<EOL
create-resource-adapter-config --threadpoolid hpfeedsra-thread-pool --property host=$HPFEEDSRA_HOST:port=$HPFEEDSRA_PORT:ident=$HPFEEDSRA_IDENT:secret=$HPFEEDSRA_SECRET:channels=$HPFEEDSRA_CHANNELS  org.honeynet.hbbackend.hpfeedsra
EOL




sudo touch /etc/_hbbackend_domain_set_up

fi

