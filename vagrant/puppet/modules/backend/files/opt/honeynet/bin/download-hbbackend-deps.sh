#!/bin/sh -e

cd /repo/libs/hpfeeds-java
mvn package
cp target/hpfeeds-java-*.jar /repo/hbbackend/lib2/
cd /repo/hbbackend
cd lib2
wget http://jdbc.postgresql.org/download/postgresql-9.1-902.jdbc4.jar
cd ..
ant -lib /usr/share/java retrieve
