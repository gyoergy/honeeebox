#!/bin/sh -e

/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.stats
/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.hpfeedsra
/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.hpfeeds
/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.shadowserver_geoip
/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.shadowserver_asn
/opt/glassfish3/bin/asadmin --port 9948 undeploy org.honeynet.hbbackend.virustotal

