#!/bin/sh -e

cd /repo/hbbackend
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.virustotal.jar
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.shadowserver_asn.jar
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.shadowserver_geoip.jar
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.hpfeedsra.rar
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.hpfeeds.jar
/opt/glassfish3/bin/asadmin --port 9948 deploy target/modules/org.honeynet.hbbackend.stats.jar
sudo touch /etc/_deploy_backend_run
