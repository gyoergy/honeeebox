#!/bin/sh -e

if [ ! -f "/opt/hbbackend/geoip/GeoLiteCity.dat" ]
then
    cd /opt/hbbackend/geoip
    wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
    gunzip GeoLiteCity.dat.gz
fi
