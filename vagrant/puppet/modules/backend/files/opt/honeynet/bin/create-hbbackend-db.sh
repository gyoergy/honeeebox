#!/bin/sh -e

# important note - this setup is only good for development!
# change these permissions and passwords for production!

if [ ! -f /etc/_hbbackend_db_created ]
then
    sudo -u postgres psql -c "create user hbbackend with password 'hbbackend';"
    sudo -u postgres psql -c "create user hbstats with password 'hbstats';"
    sudo -u postgres psql -c "create database hbbackend owner hbbackend;"

    PGPASSWORD=hbbackend psql -h localhost -U hbbackend -c "create schema hbbackend;"
    PGPASSWORD=hbbackend psql -h localhost -U hbbackend < /repo/hbbackend/schema/hbbackend.sql
    PGPASSWORD=hbbackend psql -h localhost -U hbbackend < /repo/hbbackend/schema/hbbackend_functions.sql
    PGPASSWORD=hbbackend psql -h localhost -U hbbackend -c "grant usage on schema hbbackend to hbstats;"
    PGPASSWORD=hbbackend psql -h localhost -U hbbackend -c "grant select on all tables in schema hbbackend to hbstats;"
    PGPASSWORD=hbbackend psql -h localhost -U hbbackend -c "grant create on database hbbackend to hbstats;"

    PGPASSWORD=hbstats psql -h localhost -U hbstats -d hbbackend -c "create schema hbstats;"
    PGPASSWORD=hbstats psql -h localhost -U hbstats -d hbbackend < /repo/hbbackend/schema/hbstats.sql
    PGPASSWORD=hbstats psql -h localhost -U hbstats -d hbbackend < /repo/hbbackend/schema/hbstats_functions.sql

    touch /etc/_hbbackend_db_created
fi

