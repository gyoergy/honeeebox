#!/bin/sh -e

if [ ! -f /etc/_glassfish_installed ]
then
    # race condition here. Don't run on shared machine
    mkdir /tmp/$$
    cd /tmp/$$
    rm -f *
    wget -O glassfish.zip http://download.java.net/glassfish/3.1.1/release/glassfish-3.1.1.zip
    unzip glassfish.zip
    mv glassfish3/* /opt/glassfish3
    chown -R root:root /opt/glassfish3
    # rm -rf /tmp/$$
    touch /etc/_glassfish_installed
fi
