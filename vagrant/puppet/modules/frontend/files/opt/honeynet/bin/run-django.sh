#!/bin/sh

cd /repo/hbwebui

python manage.py syncdb

echo "Basic setup done"
echo "Starting server"
echo ""
echo "If this is the first run, connect to localhost:8080/admin/ and add a sensor/group mapping"
echo ""
python manage.py runserver 0.0.0.0:8080


