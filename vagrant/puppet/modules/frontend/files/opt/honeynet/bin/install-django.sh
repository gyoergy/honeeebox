#!/bin/sh

# this is a throw-away VM so we don't bother with virtualenv

easy_install pip
pip install Django
pip install psycopg2

echo create database hbwebui_django | sudo su - postgres -c psql
echo "create user hbwebui_django password 'hbwebui_django';" | sudo su - postgres -c psql 

touch /etc/_django_installed