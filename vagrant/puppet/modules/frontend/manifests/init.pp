# install all the stuff for the web ui
#
# for now, assume that the hbackend and hbwebui dbs are
# on the same instance
#
# to split this up, just change django config file to point at the backend dbs
#

# Class: frontend::file
#
#
class frontend::files {

  # we can't easily use recuse here as backend already control 
  # the dir so just drop the files in 

  File {
    ensure => file,
    mode => 0755,
    owner => "root",
    group => "root",    
  }

  file { "/opt/honeynet/bin/install-django.sh":
    source => "puppet:///modules/frontend/opt/honeynet/bin/install-django.sh"
  }

  file { "/opt/honeynet/bin/run-django.sh":
      source => "puppet:///modules/frontend/opt/honeynet/bin/run-django.sh"
  }
}

class frontend {

    require base::postgres
    require frontend::files

    package {  ["python-psycopg2", "python-setuptools", "python-dev"]: 
        ensure => present 
    }  

    exec { "/opt/honeynet/bin/install-django.sh":
        command => "/opt/honeynet/bin/install-django.sh",
        timeout => 600,
        creates => "/etc/_django_installed"
    }
}
